package com.kyovo

import com.kyovo.adapters.FileEmployeeRepository
import com.kyovo.adapters.SpringEmailService
import com.kyovo.use_cases.BirthdayGreetings
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.io.File
import java.time.LocalDate

@SpringBootApplication
open class SpringApplication : CommandLineRunner
{
    private val EMPLOYEE_FILE_NAME = "application/src/main/resources/employees.csv"
    private val EMPLOYEES_FILE_PATH = File(EMPLOYEE_FILE_NAME).toPath()

    @Autowired
    private lateinit var emailService: SpringEmailService

    override fun run(vararg args: String?)
    {
        val birthdayGreetings = BirthdayGreetings(FileEmployeeRepository(EMPLOYEES_FILE_PATH), emailService)

        birthdayGreetings.send(LocalDate.of(2023, 5, 9))
    }
}

fun main()
{
    runApplication<SpringApplication>()
}