package com.kyovo

import com.kyovo.doubles.EmailServiceSpy
import com.kyovo.doubles.InMemoryEmployeeRepository
import com.kyovo.ports.primary.SendGreetings
import com.kyovo.ports.secondary.EmailService
import com.kyovo.ports.secondary.EmployeeRepository
import com.kyovo.tools.AnEmployee
import com.kyovo.use_cases.BirthdayGreetings
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import java.time.LocalDate
import java.time.Month.*

class BirthdayGreetingsTest
{
    private val dummyDate = LocalDate.now()
    private val emailService: EmailService = EmailServiceSpy()
    private val emailServiceSpy = emailService as EmailServiceSpy

    @Test
    fun `given empty list of employees then no email sent`()
    {
        val employeeRepository: EmployeeRepository = InMemoryEmployeeRepository()
        val birthdayGreetings: SendGreetings = BirthdayGreetings(employeeRepository, emailService)

        birthdayGreetings.send(dummyDate)

        val notifiedEmails = emailServiceSpy.getNotifiedEmails()
        assertAll(
            { assertThat(emailServiceSpy.getSentEmailsCount()).isEqualTo(0) },
            { assertThat(notifiedEmails).isEmpty() }
        )
    }

    @Test
    fun `given employees with no birthday then no email sent`()
    {
        val employeeRepository: EmployeeRepository = InMemoryEmployeeRepository(
            AnEmployee.bornOn(1990, JANUARY, 21).build(),
            AnEmployee.bornOn(1992, APRIL, 13).build()
        )
        val birthdayGreetings: SendGreetings = BirthdayGreetings(employeeRepository, emailService)

        val today = LocalDate.of(2022, DECEMBER, 11)
        birthdayGreetings.send(today)

        val notifiedEmails = emailServiceSpy.getNotifiedEmails()
        assertAll(
            { assertThat(emailServiceSpy.getSentEmailsCount()).isEqualTo(0) },
            { assertThat(notifiedEmails).isEmpty() }
        )
    }

    @Test
    fun `given one employee birthday then one email sent`()
    {
        val employeeRepository: EmployeeRepository = InMemoryEmployeeRepository(
            AnEmployee.bornOn(1990, JANUARY, 21).email("first@mail.com").build()
        )
        val birthdayGreetings: SendGreetings = BirthdayGreetings(employeeRepository, emailService)

        val today = LocalDate.of(2022, JANUARY, 21)
        birthdayGreetings.send(today)

        val notifiedEmails = emailServiceSpy.getNotifiedEmails()
        assertAll(
            { assertThat(emailServiceSpy.getSentEmailsCount()).isEqualTo(1) },
            { assertThat(notifiedEmails).containsExactly("first@mail.com") }
        )
    }

    @Test
    fun `given many employees birthday then many emails sent`()
    {
        val employeeRepository: EmployeeRepository = InMemoryEmployeeRepository(
            AnEmployee.bornOn(1990, JANUARY, 25).email("first@mail.com").build(),
            AnEmployee.bornOn(1997, MARCH, 18).email("second@mail.com").build(),
            AnEmployee.bornOn(1996, JANUARY, 25).email("third@mail.com").build(),
            AnEmployee.bornOn(2001, JANUARY, 25).email("fourth@mail.com").build()
        )
        val birthdayGreetings: SendGreetings = BirthdayGreetings(employeeRepository, emailService)

        val today = LocalDate.of(2023, JANUARY, 25)
        birthdayGreetings.send(today)

        val notifiedEmails = emailServiceSpy.getNotifiedEmails()
        assertAll(
            { assertThat(emailServiceSpy.getSentEmailsCount()).isEqualTo(3) },
            { assertThat(notifiedEmails).containsExactly("first@mail.com", "third@mail.com", "fourth@mail.com") }
        )
    }
}