package com.kyovo.doubles

import com.kyovo.Employee
import com.kyovo.ports.secondary.EmailService

class EmailServiceSpy : EmailService
{
    private val notifiedEmployees = mutableListOf<Employee>()

    override fun sendEmail(employees: List<Employee>)
    {
        notifiedEmployees.addAll(employees)
    }

    fun getSentEmailsCount(): Int = notifiedEmployees.count()

    fun getNotifiedEmails(): List<String> = notifiedEmployees.map { it.email.value }
}
