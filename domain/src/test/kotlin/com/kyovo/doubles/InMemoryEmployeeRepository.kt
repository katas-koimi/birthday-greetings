package com.kyovo.doubles

import com.kyovo.Employee
import com.kyovo.ports.secondary.EmployeeRepository
import java.time.Month

class InMemoryEmployeeRepository(private vararg val employees: Employee) : EmployeeRepository
{
    override fun findEmployeesBornOn(month: Month, day: Int): List<Employee> =
            employees.filter { employee -> employee.bornOn(month, day) }
}
