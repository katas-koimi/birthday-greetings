package com.kyovo

import java.time.LocalDate
import java.time.Month
import java.time.Year
import java.time.format.DateTimeFormatter

data class BirthDate(val day: Day, val month: Month, val year: Year)
{
    fun isBirthday(month: Month, day: Int) = this.day == Day(day) && this.month == month

    companion object
    {
        fun parse(date: String, pattern: DateTimeFormatter): BirthDate
        {
            val localDate = LocalDate.parse(date, pattern)
            return BirthDate(Day(localDate.dayOfMonth), localDate.month, Year.of(localDate.year))
        }
    }
}

@JvmInline
value class Day(val value: Int)