package com.kyovo

import java.time.Month

data class Employee(val lastName: LastName, val firstName: FirstName, val dateOfBirth: BirthDate,
                    val email: EmailAddress)
{
    fun bornOn(month: Month, day: Int): Boolean = dateOfBirth.isBirthday(month, day)
}

@JvmInline
value class LastName(val value: String)

@JvmInline
value class FirstName(val value: String)

@JvmInline
value class EmailAddress(val value: String)