package com.kyovo.use_cases

import com.kyovo.ports.primary.SendGreetings
import com.kyovo.ports.secondary.EmailService
import com.kyovo.ports.secondary.EmployeeRepository
import java.time.LocalDate

class BirthdayGreetings(private val employeeRepository: EmployeeRepository, private val emailService: EmailService) :
    SendGreetings
{
    override fun send(date: LocalDate)
    {
        val birthdayEmployees = employeeRepository.findEmployeesBornOn(date.month, date.dayOfMonth)
        emailService.sendEmail(birthdayEmployees)
    }
}
