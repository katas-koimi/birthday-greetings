package com.kyovo.ports.primary

import java.time.LocalDate

interface SendGreetings
{
    fun send(date: LocalDate)
}
