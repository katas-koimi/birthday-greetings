package com.kyovo.ports.secondary

import com.kyovo.Employee

interface EmailService
{
    fun sendEmail(employees: List<Employee>)
}
