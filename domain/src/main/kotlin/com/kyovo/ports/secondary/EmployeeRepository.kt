package com.kyovo.ports.secondary

import com.kyovo.Employee
import java.time.Month

interface EmployeeRepository
{
    fun findEmployeesBornOn(month: Month, day: Int): List<Employee>
}