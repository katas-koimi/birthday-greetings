package com.kyovo.exceptions

import java.io.FileNotFoundException
import java.nio.file.Path
import kotlin.io.path.absolute

class EmployeesFileNotFoundException(fileName: Path) :
    FileNotFoundException("Cannot find employees file '${fileName.absolute()}'")
