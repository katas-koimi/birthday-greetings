package com.kyovo.tools

import com.kyovo.*
import java.time.Month
import java.time.Month.JANUARY
import java.time.Year

object AnEmployee
{
    private var dummyEmployee = Employee(
        LastName("dummyLastName"),
        FirstName("dummyFirstName"),
        BirthDate(Day(1), JANUARY, Year.of(1990)),
        EmailAddress("dummy@email.com")
    )

    fun firstName(firstName: String) = apply {
        dummyEmployee = dummyEmployee.copy(firstName = FirstName(firstName))
    }

    fun bornOn(year: Int, month: Month, day: Int) = apply {
        dummyEmployee = dummyEmployee.copy(dateOfBirth = BirthDate(Day(day), month, Year.of(year)))
    }

    fun email(email: String) = apply {
        dummyEmployee = dummyEmployee.copy(email = EmailAddress(email))
    }

    fun build(): Employee = dummyEmployee
}