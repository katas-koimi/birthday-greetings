package com.kyovo.adapters

import com.kyovo.Employee
import com.kyovo.ports.secondary.EmailService
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

@Service
class SpringEmailService(private val mailSender: JavaMailSender) : EmailService
{
    override fun sendEmail(employees: List<Employee>)
    {
        val messages = mutableListOf<SimpleMailMessage>()

        employees.forEach { employee ->
            messages.add(messageFor(employee))
        }

        mailSender.send(*messages.toTypedArray())
    }

    private fun messageFor(employee: Employee): SimpleMailMessage
    {
        val message = SimpleMailMessage()

        message.subject = "Happy birthday!"
        message.setTo(employee.email.value)
        message.text = "Happy birthday, dear ${employee.firstName.value}!"

        return message
    }
}