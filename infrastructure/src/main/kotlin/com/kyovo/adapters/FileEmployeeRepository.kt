package com.kyovo.adapters

import com.kyovo.*
import com.kyovo.exceptions.EmployeesFileNotFoundException
import com.kyovo.ports.secondary.EmployeeRepository
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.nio.file.Path
import java.time.Month
import java.time.format.DateTimeFormatter

class FileEmployeeRepository(private val file: Path) : EmployeeRepository
{
    private companion object
    {
        const val FILE_DELIMITER = ", "
        const val DATE_FORMAT = "yyyy/MM/dd"
        private val dateOfBirthPattern = DateTimeFormatter.ofPattern(DATE_FORMAT)
    }

    override fun findEmployeesBornOn(month: Month, day: Int): List<Employee>
    {
        val fileEmployees: List<FileEmployee> = readEmployeesFromFile()
        val employees = fileEmployees.map { it.toEmployee() }

        return employees.filter { employee -> employee.bornOn(month, day) }
    }

    private fun readEmployeesFromFile(): List<FileEmployee>
    {
        try
        {
            val inputStream: FileInputStream = file.toFile().inputStream()
            val bufferedReader: BufferedReader = inputStream.bufferedReader()
            skipHeader(bufferedReader)

            val fileEmployees = parse(bufferedReader.lineSequence())
            bufferedReader.close()

            return fileEmployees
        }
        catch (exception: FileNotFoundException)
        {
            throw EmployeesFileNotFoundException(file)
        }
    }

    private fun skipHeader(bufferedReader: BufferedReader)
    {
        bufferedReader.readLine()
    }

    private fun parse(lineSequence: Sequence<String>): List<FileEmployee>
    {
        val fileEmployees = mutableListOf<FileEmployee>()
        lineSequence.forEach { line ->
            val (lastName, firstName, dateOfBirth, email) = line.split(FILE_DELIMITER)
            fileEmployees.add(FileEmployee(lastName, firstName, dateOfBirth, email))
        }
        return fileEmployees
    }

    private fun FileEmployee.toEmployee(): Employee = Employee(
        LastName(lastName),
        FirstName(firstName),
        BirthDate.parse(dateOfBirth, dateOfBirthPattern),
        EmailAddress(email)
    )
}
