package com.kyovo.integration

import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.GreenMailUtil
import com.icegreen.greenmail.util.ServerSetupTest
import com.kyovo.adapters.SpringEmailService
import com.kyovo.tools.AnEmployee
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.springframework.mail.javamail.JavaMailSenderImpl

class SpringEmailServiceTest
{
    companion object
    {
        private lateinit var greenMail: GreenMail
        private lateinit var emailService: SpringEmailService

        @JvmStatic
        @BeforeAll
        fun setUp()
        {
            greenMail = GreenMail(ServerSetupTest.SMTP)
            greenMail.start()

            configureJavaMailSenderToUseGreenMail()
        }

        private fun configureJavaMailSenderToUseGreenMail()
        {
            val javaMailSender = JavaMailSenderImpl()
            javaMailSender.host = ServerSetupTest.SMTP.bindAddress
            javaMailSender.port = ServerSetupTest.SMTP.port

            emailService = SpringEmailService(javaMailSender)
        }

        @JvmStatic
        @AfterAll
        fun tearDown()
        {
            greenMail.stop()
        }
    }

    @AfterEach
    fun purgeMailBox()
    {
        greenMail.purgeEmailFromAllMailboxes()
    }

    @Test
    fun `given empty employees then no mail sent`()
    {
        emailService.sendEmail(emptyList())

        val receivedMessages = greenMail.receivedMessages

        assertThat(receivedMessages.size).isEqualTo(0)
    }

    @Test
    fun `given one employee then one mail sent`()
    {
        emailService.sendEmail(
            listOf(
                AnEmployee.firstName("John").email("john.doe@foobar.com").build()
            )
        )

        val receivedMessages = greenMail.receivedMessages

        assertAll(
            { assertThat(receivedMessages.size).isEqualTo(1) },
            { assertThat(receivedMessages[0].allRecipients.size).isEqualTo(1) },
            { assertThat(receivedMessages[0].allRecipients[0].toString()).isEqualTo("john.doe@foobar.com") },
            { assertThat(receivedMessages[0].subject).isEqualTo("Happy birthday!") },
            { assertThat(GreenMailUtil.getBody(receivedMessages[0])).isEqualTo("Happy birthday, dear John!") }
        )
    }

    @Test
    fun `given one employee then one mail sent (other case)`()
    {
        emailService.sendEmail(
            listOf(
                AnEmployee.firstName("Ann").email("ann.mary@foobar.com").build()
            )
        )

        val receivedMessages = greenMail.receivedMessages

        assertAll(
            { assertThat(receivedMessages.size).isEqualTo(1) },
            { assertThat(receivedMessages[0].allRecipients.size).isEqualTo(1) },
            { assertThat(receivedMessages[0].allRecipients[0].toString()).isEqualTo("ann.mary@foobar.com") },
            { assertThat(receivedMessages[0].subject).isEqualTo("Happy birthday!") },
            { assertThat(GreenMailUtil.getBody(receivedMessages[0])).isEqualTo("Happy birthday, dear Ann!") }
        )
    }

    @Test
    fun `given several employees then several mails sent`()
    {
        emailService.sendEmail(
            listOf(
                AnEmployee.firstName("Tom").email("tom.t@foobar.com").build(),
                AnEmployee.firstName("Carol").email("carol.c@foobar.com").build(),
                AnEmployee.firstName("Jessie").email("jessie.j@foobar.com").build()
            )
        )

        val receivedMessages = greenMail.receivedMessages
        val firstMessage = receivedMessages[0]
        val secondMessage = receivedMessages[1]
        val thirdMessage = receivedMessages[2]

        assertAll(
            { assertThat(receivedMessages.size).isEqualTo(3) },
            {
                assertThat(receivedMessages.map { it.subject }).containsExactly(
                    "Happy birthday!",
                    "Happy birthday!",
                    "Happy birthday!"
                )
            },

            { assertThat(firstMessage.allRecipients.size).isEqualTo(1) },
            { assertThat(firstMessage.allRecipients[0].toString()).isEqualTo("tom.t@foobar.com") },
            { assertThat(GreenMailUtil.getBody(firstMessage)).isEqualTo("Happy birthday, dear Tom!") },

            { assertThat(secondMessage.allRecipients.size).isEqualTo(1) },
            { assertThat(secondMessage.allRecipients[0].toString()).isEqualTo("carol.c@foobar.com") },
            { assertThat(GreenMailUtil.getBody(secondMessage)).isEqualTo("Happy birthday, dear Carol!") },

            { assertThat(thirdMessage.allRecipients.size).isEqualTo(1) },
            { assertThat(thirdMessage.allRecipients[0].toString()).isEqualTo("jessie.j@foobar.com") },
            { assertThat(GreenMailUtil.getBody(thirdMessage)).isEqualTo("Happy birthday, dear Jessie!") }
        )
    }
}