package com.kyovo.integration

import com.kyovo.*
import com.kyovo.adapters.FileEmployeeRepository
import com.kyovo.ports.secondary.EmployeeRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.FileNotFoundException
import java.nio.file.Files
import java.nio.file.Path
import java.time.Month.*
import java.time.Year
import kotlin.io.path.deleteIfExists

class FileEmployeeRepositoryTest
{
    companion object
    {
        private const val FILE_NAME = "employees"
        private const val FILE_EXTENSION = ".csv"
        private val file: Path = Files.createTempFile(FILE_NAME, FILE_EXTENSION)

        @JvmStatic
        @AfterAll
        fun tearDown()
        {
            file.deleteIfExists()
        }
    }

    @Test
    fun `given empty file with only header then get empty list of employees`()
    {
        givenFileContent("last_name, first_name, date_of_birth, email")
        val employeeRepository: EmployeeRepository = FileEmployeeRepository(file)

        val employees = employeeRepository.findEmployeesBornOn(JANUARY, 1)

        assertThat(employees).isEmpty()
    }

    @Test
    fun `given file with one birthday employee then get that employee`()
    {
        givenFileContent(
            "last_name, first_name, date_of_birth, email",
            "Doe, John, 1982/10/08, john.doe@foobar.com"
        )
        val employeeRepository: EmployeeRepository = FileEmployeeRepository(file)

        val employees = employeeRepository.findEmployeesBornOn(OCTOBER, 8)

        assertThat(employees).containsExactly(
            Employee(
                LastName("Doe"),
                FirstName("John"),
                BirthDate(Day(8), OCTOBER, Year.of(1982)),
                EmailAddress("john.doe@foobar.com")
            )
        )
    }

    @Test
    fun `given file with one birthday employee then get that employee (other case)`()
    {
        givenFileContent(
            "last_name, first_name, date_of_birth, email",
            "Ann, Mary, 1975/09/11, mary.ann@foobar.com"
        )
        val employeeRepository: EmployeeRepository = FileEmployeeRepository(file)

        val employees = employeeRepository.findEmployeesBornOn(SEPTEMBER, 11)

        assertThat(employees).containsExactly(
            Employee(
                LastName("Ann"),
                FirstName("Mary"),
                BirthDate(Day(11), SEPTEMBER, Year.of(1975)),
                EmailAddress("mary.ann@foobar.com")
            )
        )
    }

    @Test
    fun `given file with many employees but few birthday then get that employees`()
    {
        givenFileContent(
            "last_name, first_name, date_of_birth, email",
            "Doe, John, 1982/10/08, john.doe@foobar.com",
            "Ann, Mary, 1975/09/11, mary.ann@foobar.com"
        )
        val employeeRepository: EmployeeRepository = FileEmployeeRepository(file)

        val employees = employeeRepository.findEmployeesBornOn(OCTOBER, 8)

        assertThat(employees).containsExactly(
            Employee(
                LastName("Doe"),
                FirstName("John"),
                BirthDate(Day(8), OCTOBER, Year.of(1982)),
                EmailAddress("john.doe@foobar.com")
            )
        )
    }

    @Test
    fun `given file with many employees but few birthday then get that employees (other case)`()
    {
        givenFileContent(
            "last_name, first_name, date_of_birth, email",
            "Doe, John, 1982/10/08, john.doe@foobar.com",
            "Ann, Mary, 1975/09/11, mary.ann@foobar.com",
            "Jack, Jones, 1977/09/11, jones.jack@foobar.com",
            "Brady, Kevin, 1990/03/28, kevin.brady@foobar.com",
            "Smith, Carol, 1998/09/11, carol.smith@foobar.com",
        )
        val employeeRepository: EmployeeRepository = FileEmployeeRepository(file)

        val employees = employeeRepository.findEmployeesBornOn(SEPTEMBER, 11)

        assertThat(employees).containsExactly(
            Employee(
                LastName("Ann"),
                FirstName("Mary"),
                BirthDate(Day(11), SEPTEMBER, Year.of(1975)),
                EmailAddress("mary.ann@foobar.com")
            ),
            Employee(
                LastName("Jack"),
                FirstName("Jones"),
                BirthDate(Day(11), SEPTEMBER, Year.of(1977)),
                EmailAddress("jones.jack@foobar.com")
            ),
            Employee(
                LastName("Smith"),
                FirstName("Carol"),
                BirthDate(Day(11), SEPTEMBER, Year.of(1998)),
                EmailAddress("carol.smith@foobar.com")
            )
        )
    }

    @Test
    fun `given non-existent file then get exception`()
    {
        val employeeRepository: EmployeeRepository = FileEmployeeRepository(Path.of("dummy.csv"))

        val exception = assertThrows<FileNotFoundException> {
            employeeRepository.findEmployeesBornOn(JANUARY, 1)
        }

        assertThat(exception.message).contains("Cannot find employees file")
    }

    private fun givenFileContent(vararg lines: String)
    {
        Files.write(file, lines.toList())
    }
}